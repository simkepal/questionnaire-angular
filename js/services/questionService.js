angular.module('questionsApp').factory('questionService', function($firebase) {
	var firebase = new Firebase('https://questionnaire.firebaseio.com/');
	var data = $firebase(firebase);

	return {
		returnData: function() {
			return data;
		},
		buildTree: function(parent) {
			var self = this;
			var tree = {};
			var keys = data.$getIndex();

			angular.forEach(keys, function(key) {
				var element = data[key];
				element["key"] = key;

				if(data[key].parent == parent) {
					var children = self.buildTree(key);
					if(children) {
						element['children'] = children;
					}


					tree[Object.keys(tree).length] = element;
				}		
			});

			return tree;
		}
	};
});