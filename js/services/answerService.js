angular.module('questionsApp').factory('answerService', function($firebase) {
	var firebase = new Firebase('https://answers.firebaseio.com/');
	var data = $firebase(firebase);

	return {
		returnData: function() {
			return data;
		}
	};
});