// Angular App
angular.module('questionsApp', [
	'ngRoute',
	'firebase'
])


.config(['$routeProvider', '$locationProvider',
	function($routeProvider, $locationProvider) {
		$locationProvider.html5Mode(true).hashPrefix('!');

		$routeProvider.
			when('/', {
				templateUrl: 'templates/questions.html',
				controller: 'questionsController'
			}).
			when('/answers',  {
				templateUrl: 'templates/answers.html',
				controller: 'answersController'
			}).
			otherwise({
				redirectTo: '/'		
			});
	}
]);