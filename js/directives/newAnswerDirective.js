angular.module('questionsApp').directive('newAnswer', function ($rootScope, $compile) {
        var countId;

        var createTemplate = function() {
                var tpl =       '<div class="form-group">';
                tpl +=                  '<label for="answer_' + countId + '">';
                tpl +=                          'Answer ' + countId;
                tpl +=                  '</label>';
                tpl +=                  '<input type="text" class="form-control" id="answer_' + countId + '" placeholder="Enter Answer ' + countId + '" ng-model="showQuestionForm.answers.' + countId + '.answer">';
                tpl +=          '</div>';

                return tpl;
        }

        var linker = function(scope, elm, attrs) {
                countId = $rootScope.countId;
                $rootScope.countId += 1;
                         
                var loader = createTemplate();
                elm.html(loader);
                elm.replaceWith($compile(elm.html())(scope));

        }

        return {
                restrict: 'A',
                link: linker
        };
});