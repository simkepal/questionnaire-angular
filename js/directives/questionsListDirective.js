angular.module('questionsApp').directive('questionsList', function ($compile) {
       
        var createTemplate = function() {
                var tpl = '     <div class="list-group">';
                tpl += '              <div class="list-group-item">';
                tpl += '                        <h4 class="list-group-item-heading">{{question.question}} <small>{{question.key}}</small></h4>';
                tpl += '                        <span class="badge">Depth: {{question.depth}}</span>';
                tpl += '                        <h6>Type: <span>{{question.type}}</span></h6>';
                tpl += '                        <h6 ng-show="question.parent">Parent: <span>{{question.parent}}</span></h6>';
                tpl += '                        <h6 ng-show="question.parent">Parent Answer: <span>{{question.parentAnswer}}</span></h6>';
                tpl += '                        <div class="list-group-item-text">';
                tpl += '                                <ul>';
                tpl += '                                        <li ng-repeat="answer in question.answers">';
                tpl += '                                                {{answer.answer}} (answer id: {{answer.id}})';
                tpl += '                                                <button class="btn btn-primary btn-xs" ng-click="newQuestion(question.depth + 1, question.key, answer.id)">Add Subquestion</button>';
                tpl += '                                        </li>';
                tpl += '                                </ul>   ';
                tpl += '                        </div>';
                tpl += '                        <button class="btn btn-primary btn-xs" ng-click="deleteQuestion(question.key)">Delete</button>';
                tpl += '                        <button class="btn btn-primary btn-xs" ng-click="editQuestion(question.key)">Edit</button>';
                // subquestion
                tpl += '                        <hr />';
                tpl += '                        <div class="question" ng-repeat="(key, question) in question.children">';
                tpl += '                                <div questions-list></div>';
                tpl += '                        </div>';
                
                tpl += '                </div>';
                tpl += '        </div>';

                return tpl;
        }

        var linker = function(scope, elm, attrs) {
                var loader = createTemplate(scope);
                elm.html(loader);
                elm.replaceWith($compile(elm.html())(scope));
        }

        return {
                restrict: 'A',
                link: linker,
        };
});