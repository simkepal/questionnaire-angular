angular.module('questionsApp').directive('question', function ($compile) {
        var loader;

        var createTemplate = function(type,addOther) {
                var tpl = "";
                switch(type) {
                        case "textField":
                                tpl += '     <div class="question">';
                                tpl += '                <h5>{{currentQuestion.question}}</h5>';
                                tpl += '                <input type="text" value="" placeholder="Answer" ng-model="answered[currentQuestion.key]">';
                                tpl += '                <button class="btn btn-primary" ng-click="nextQuestion();">Next</button>';
                                tpl += '        </div>';
                                
                                break;

                        case "stars":
                                tpl += '     <div class="question">';
                                tpl += '                <h5>{{currentQuestion.question}}</h5>';
                                tpl += '                stars';
                                tpl += '                <button class="btn btn-primary" ng-click="nextQuestion();">Next</button>';
                                tpl += '        </div>';
                                
                                break;

                        case "radio":
                                tpl += '     <div class="question">';
                                tpl += '                <h5>{{currentQuestion.question}}</h5>';

                                tpl += '                <div class="form-group" ng-repeat="answer in currentQuestion.answers">';
                                tpl += '                        <div class="radio">';
                                tpl += '                                <label class="radio-inline control-label">';
                                tpl += '                                        <input type="radio" name="{{currentQuestion.key}}" ng-value="{{answer.id}}" ng-model="answered[currentQuestion.key]">{{answer.answer}}';
                                tpl += '                                </label>';
                                tpl += '                        </div>';
                                tpl += '                </div>';

                
                                if(addOther) {
                                        tpl += '                <div class="form-group">';
                                        tpl += '                        <div class="radio">';
                                        tpl += '                                <label class="radio-inline control-label">';
                                        tpl += '                                        <input type="radio" name="{{currentQuestion.key}}" ng-value="other" ng-model="answered[currentQuestion.key]">Other <br />';
                                        tpl += '                                        <input placeholder="Your answer" class="form-control" type="text" value="" ng-model="other">'
                                        tpl += '                                </label>';
                                        tpl += '                        </div>';
                                        tpl += '                </div>';
                                }

                                tpl += '                <button class="btn btn-primary" ng-click="nextQuestion();">Next</button>';
                                tpl += '        </div>';
                                
                                break;

                        case "select":
                                tpl += '        <div class="question">';
                                tpl += '                <h5>{{currentQuestion.question}}</h5>';
                                tpl += '                <div class="form-group">';
                                tpl += '                        <select class="form-control" ng-model="answered[currentQuestion.key]">';
                                tpl += '                                <option ng-repeat="answer in currentQuestion.answers" value="{{answer.id}}">{{answer.answer}}</option>';
                                if(addOther) {
                                        tpl += '                        <option value="other">Other</option>';
                                }

                                tpl += '                        </select>';
                                tpl += '                </div>';

                                tpl += '                <button class="btn btn-primary" ng-click="nextQuestion();">Next</button>';
                                tpl += '        </div>';

                                break;

                        default:
                                console.log("error");

                                break;
                }
                

                return tpl;
        }

        var linker = function(scope, elm, attrs) {
                scope.$watch("currentQuestion", function(currentQuestion) {
                        if(currentQuestion) {
                                loader = createTemplate(currentQuestion.type, currentQuestion.addOther);
                             
                                elm.html(loader);
                                elm.html($compile(elm.html())(scope));
                        }
                });
        }

        return {
                restrict: 'A',
                link: linker
        };
});
