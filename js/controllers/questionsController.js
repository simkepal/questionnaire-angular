angular.module('questionsApp').controller('questionsController', [
'$rootScope',
'$scope',
'questionService',
function ($rootScope, $scope, questionService) {

	// var firebase = new Firebase('https://questionnaire.firebaseio.com/');
	var isLoaded = false;

	$scope.questions = questionService.returnData();
	$scope.showQuestionForm = false;
	$rootScope.countId  = 0;
	$scope.tree;
	$scope.update = false;

	// when data is loaded build the tree of question
	$scope.questions.$on("loaded", function() {		
		$scope.tree = questionService.buildTree(0);
		isLoaded = true;
	});

	// when child added rebuild the tree
	$scope.questions.$on('child_added', function() {
		if(!isLoaded) {
			return;
		}
		
		$scope.tree = questionService.buildTree(0);
	});

	// when data is removed rebuild the tree
	$scope.questions.$on('child_removed', function() {
		if(!isLoaded) {
			return;
		}
		
		$scope.tree = questionService.buildTree(0);
	});


	// create new question
	$scope.newQuestion = function(depth, parent, parentAnswer) {
		$scope.update = false;
		var keys = $scope.questions.$getIndex();

		$scope.showQuestionForm = {
			questionId: keys.length ? keys.length : 0,
			answers: [],
			depth: depth,
			parent: parent,
			parentAnswer: parentAnswer
		}
	}

	// submit question
	$scope.processQuestion = function() {
		if($scope.update) {
			$scope.questions.$save($scope.update);
		}
		else {
			$scope.questions.$add($scope.showQuestionForm);
		}

		// reset
		$scope.showQuestionForm = false;
		$rootScope.countId  = 0;
	}

	// add answer
	$scope.addAnswer = function(){
		var answer = {
                        id: $scope.showQuestionForm.answers instanceof Array ? $scope.showQuestionForm.answers.length : 0,
                        answer: "",
                };

                if(!($scope.showQuestionForm.answers instanceof Array)) {
                	$scope.showQuestionForm.answers = [];
                }
                $scope.showQuestionForm.answers.push(answer);
	}

	// delete question and subquestions
	$scope.deleteQuestion = function(key) {
		// find child of current question and delete it
		if($scope.questions[key].children instanceof Object && Object.keys($scope.questions[key].children).length > 0) {
			angular.forEach($scope.questions[key].children, function(value) {
				$scope.deleteQuestion(value.key);
			});
		}
		$scope.questions.$remove(key);
	}

	// edit question
	$scope.editQuestion = function(key) {
		$scope.update = key;
		$scope.showQuestionForm = false;
		$rootScope.countId  = 0;
		$scope.showQuestionForm = $scope.questions[key];
	}

	// reset question
	$scope.resetQuestion = function() {
		isLoaded = false;
		$scope.showQuestionForm = false;
		$rootScope.countId  = 0;

		$scope.questions = questionService.returnData();

		$scope.questions.$on("loaded", function() {
			$scope.tree = questionService.buildTree(0);
			console.log($scope.tree);
			isLoaded = true;
		});
	}
}]);
