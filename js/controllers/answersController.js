angular.module('questionsApp').controller('answersController',  [
'$rootScope',
'$scope',
'questionService',
'answerService',
function ($rootScope, $scope, questionService, answerService) {
	$scope.questions = questionService.returnData();
	$scope.answers = answerService.returnData();
	$scope.tree;
	$scope.answered = {};
	$scope.questionsLoaded = false;
	$scope.currentQuestionNb = 0;
	$scope.currentQuestion;

	var treeLength;

	$scope.answers.$on("loaded", function() {
		
	});

	$scope.questions.$on("loaded", function() {
		$scope.tree = questionService.buildTree(0);
		$scope.questionsLoaded = true;
		treeLength = Object.keys($scope.tree).length;
	});

	$scope.start = function() {
		$scope.currentQuestion = $scope.tree[$scope.currentQuestionNb];
	}

	$scope.nextQuestion = function() {
		// find subquestion
		if(Object.keys($scope.currentQuestion.children).length > 0) {
			var foundSubquestion = false;
			angular.forEach($scope.currentQuestion.children, function(value, index) {
				if(value.parentAnswer == $scope.answered[$scope.currentQuestion.key]) {
					$scope.currentQuestion = $scope.questions[value.key];
					$scope.currentQuestion["key"] = value.key;
					console.log($scope.currentQuestion);
					foundSubquestion = true;
					return;
				}
			});

			if(foundSubquestion) {
				return;
			}
		}

		$scope.currentQuestionNb += 1;
		
		if($scope.currentQuestionNb >= treeLength) {
			$scope.process();
			return;
		}

		$scope.currentQuestion = $scope.tree[$scope.currentQuestionNb];

	}

	$scope.process = function() {
		$scope.currentQuestion = false;
		$scope.answers.$add($scope.answered);
	}
}]);
